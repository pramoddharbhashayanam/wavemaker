Application.$controller("MainPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */

        $scope.Variables.CurrentDate.dataSet.dataValue = Date.parse(new Date().toDateString());
        var d = new Date();
        d.setMonth(d.getMonth() + 2);
        $scope.Variables.MaxDate.dataSet.dataValue = Date.parse(d.toDateString());


        if ($scope.Variables.approvedTripT.dataSet.dataValue == "") {
            $scope.Variables.getApprovedRequests.update();
        } else {
            if ($scope.Variables.approvedTripT.dataSet.dataValue.content.length > 0)
                assignData();
            else {
                noRequests();
            }
        }

        if ($scope.Variables.empObj.dataSet.empId == "") {

            $scope.Variables.getLoggedInUserObject.update();
        }


        var carousel = document.querySelector('[uib-carousel]');
        var carouselScope = $('[data-identifier="carousel"] [no-wrap="noWrapSlides"]').isolateScope();

        Hammer(carousel).on("swipeleft", function() {
            carouselScope.next();
            carouselScope.$apply();
        });

        Hammer(carousel).on("swiperight", function() {
            carouselScope.prev();
            carouselScope.$apply();
        });
    };



    $scope.getLoggedInUserObjectonSuccess = function(variable, data) {
        $scope.Variables.empObj.dataSet = data;
    };

    $scope.getApprovedRequestsonSuccess = function(variable, data) {

        $scope.Variables.approvedTripT.dataSet.dataValue = data;
        if (data.content.length == 0) {
            noRequests();
        } else {
            assignData();
        }
    };

    function noRequests() {
        $scope.Widgets.tripDetails.show = false;
        $scope.Widgets.noTripsContainer.show = true;
        $scope.Widgets.addTripFloatingButton.show = true;
    }

    function assignData() {

        $scope.Widgets.tripDetails.show = true;
        $scope.Widgets.noTripsContainer.show = false;
        $scope.Widgets.addTripFloatingButton.show = false;
    }


}]);