var _WM_APP_PROPERTIES = {
  "activeTheme" : "travel-advisor",
  "defaultLanguage" : "en",
  "displayName" : "Travel Assist",
  "homePage" : "Main",
  "name" : "Travel_Assist",
  "platformType" : "MOBILE",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};